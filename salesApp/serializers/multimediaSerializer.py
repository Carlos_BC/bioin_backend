from rest_framework import serializers
from salesApp.models.multimedia import Multimedia


class MultimediaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Multimedia
        fields = ['id', 'name', 'path', 'idProduct']
