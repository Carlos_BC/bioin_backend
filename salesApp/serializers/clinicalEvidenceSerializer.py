from rest_framework import serializers
from salesApp.models.clinical_evidence import ClinicalEvidence

class ClinicalEvidenceSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClinicalEvidence
        fields = ['id', 'name', 'path', 'idProduct']