from rest_framework import serializers
from salesApp.models.document import Document


class DocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Document
        fields = ['id', 'name', 'path', 'idProduct']
