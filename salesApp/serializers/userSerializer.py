from rest_framework import serializers
from salesApp.models.user import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id','username','password','name','surname','email']
        '''
    def create(self, validated_data):
        roleData = validated_data.pop('idRole')
        roleInstance = Role.objects.create(**roleData)
        
        userInstance = User.objects.create(**validated_data, idRole = roleInstance.id)
        return userInstance
        '''
    