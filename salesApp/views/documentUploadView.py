from rest_framework import views, status
from salesApp.serializers.documentSerializer import DocumentSerializer
from rest_framework.response import Response

class DocumentUploadView(views.APIView):
    def post(self, request, *args, **kwargs):
        serializer = DocumentSerializer(data = request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(data={"message": "Documento subido"}, status=status.HTTP_201_CREATED)