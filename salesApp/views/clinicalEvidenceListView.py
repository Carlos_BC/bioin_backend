from salesApp.models.clinical_evidence import ClinicalEvidence
from salesApp.models.category import Category
from salesApp.models.product import Product
from rest_framework import status, views
from rest_framework.response import Response
from salesApp.serializers.clinicalEvidenceSerializer import ClinicalEvidenceSerializer


class ClinicalEvidenceListView(views.APIView):

    def get(self, request, *args, **kwargs):
        """ List all the clinical evidence files, related with a specific product """
        
        
        category_id = Category.objects.filter(name = kwargs['name_category']).values()[0]["id"]
        product_id = Product.objects.filter(name = kwargs['name_product'], idCategory_id= category_id).values()[0]["id"]
        clinical_evidence = ClinicalEvidence.objects.filter(idProduct = product_id).values()
        return Response({'data': clinical_evidence}, status = status.HTTP_200_OK)
