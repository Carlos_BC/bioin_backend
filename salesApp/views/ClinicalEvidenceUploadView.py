from rest_framework import views
from salesApp.serializers.clinicalEvidenceSerializer import ClinicalEvidenceSerializer
from rest_framework.response import Response


class ClinicalEvidenceUploadView(views.APIView):
    def post(self, request, *args, **kwargs):
        serializer = ClinicalEvidenceSerializer(data = request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(data={"message": "Evidencia Clinica Subida"})