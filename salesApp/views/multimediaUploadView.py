from rest_framework import serializers, views
from salesApp.models.multimedia import Multimedia
from salesApp.serializers.multimediaSerializer import MultimediaSerializer
from rest_framework.response import Response

class MultimediaUploadView(views.APIView):
    def post(self, request, *args, **kwargs):
        serializer = MultimediaSerializer(data = request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(data= {"message": "Multimedia subido"})
