from salesApp.models.document import Document
from salesApp.models.category import Category
from salesApp.models.product import Product
from rest_framework import status, views
from rest_framework.response import Response
#from salesApp.serializers.documentSerializer import DocumentSerializer


class DocumentListView(views.APIView):

    def get(self, request, *args, **kwargs):
        category_id = Category.objects.filter(name = kwargs['name_category']).values()[0]["id"]
        product_id = Product.objects.filter(name = kwargs['name_product'], idCategory_id= category_id).values()[0]["id"]
        document = Document.objects.filter(idProduct = product_id).values()
        return Response({'data': document}, status = status.HTTP_200_OK)