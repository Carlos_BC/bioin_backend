from salesApp.models.product import Product
from salesApp.models.product import Category
from rest_framework import status, views
from rest_framework.response import Response
from salesApp.serializers.productSerializer import ProductSerializer


class ProductListView(views.APIView):

    def get(self, request, *args, **kwargs):
        """ List all products realated with a specific category """
        category_id = Category.objects.filter(name = kwargs['name_category']).values()[0]["id"] ##It's a List
        products_by_categorie = Product.objects.filter(idCategory = category_id).values()   

        return Response({'data': products_by_categorie}, status = status.HTTP_200_OK)
