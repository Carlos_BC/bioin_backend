from salesApp import serializers
from salesApp.models.category import Category
from rest_framework import status, generics,views
from rest_framework.response import Response
from salesApp.serializers.categorySerializer import CategorySerializer

from rest_framework.renderers import JSONRenderer
import json
class CategoryListView(views.APIView):

    def get(self, request, *args, **kwargs):
        """ List all the categories """

        # Returns a List of dictionaries
        categories = Category.objects.all().values()

        return Response(data = {"data": categories}, status = status.HTTP_200_OK)
