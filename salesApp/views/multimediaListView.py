from salesApp.models.multimedia import Multimedia
from salesApp.models.product import Product
from salesApp.models.category import Category
from rest_framework import status, views
from rest_framework.response import Response
#from salesApp.serializers.documentSerializer import DocumentSerializer


class MultimediaListView(views.APIView):

    def get(self, request, *args, **kwargs):
        category_id = Category.objects.filter(name = kwargs['name_category']).values()[0]["id"]
        product_id = Product.objects.filter(name = kwargs['name_product'], idCategory_id= category_id).values()[0]["id"]
        multimedia = Multimedia.objects.filter(idProduct = product_id).values()
        return Response({'data': multimedia}, status = status.HTTP_200_OK)