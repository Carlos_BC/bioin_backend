from rest_framework import serializers, status, views
from salesApp.serializers.userSerializer import UserSerializer
from rest_framework.response import Response


class UserSignInView(views.APIView):
    def post(self, request, *args, **kwargs):
        serializer = UserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(data={"message": "Se ha creado el usuario"}, status=status.HTTP_201_CREATED)
