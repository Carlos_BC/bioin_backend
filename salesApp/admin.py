from django.contrib import admin
from .models.user import User
from .models.product import Product
from .models.category import Category
from .models.document import Document
from .models.clinical_evidence import ClinicalEvidence
from .models.multimedia import Multimedia


admin.site.register(User)
admin.site.register(Category)
admin.site.register(Product)
admin.site.register(Multimedia)
admin.site.register(ClinicalEvidence)
admin.site.register(Document)



