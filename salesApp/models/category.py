from django.db import models

class Category(models.Model):
    id = models.AutoField(primary_key = True)
    name = models.CharField('Name', max_length= 60, unique = True)
    imagePath = models.CharField(max_length=300,null = True, blank= True, editable=True)
