from django.db import models
from .product import Product

class ClinicalEvidence(models.Model):
    id = models.AutoField(primary_key = True)
    name = models.CharField('Name', max_length= 60, unique = True)
    path = models.CharField(max_length=300,null = True, blank= True)
    idProduct = models.ForeignKey(Product, related_name='clinical_evidence', on_delete=models.CASCADE)