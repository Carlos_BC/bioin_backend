from django.db import models
from .category import Category


class Product(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField('Name', max_length=60, unique=True)
    imagePath = models.CharField(max_length=300,null = True, blank= True)
    idCategory = models.ForeignKey(
        Category, related_name='product', on_delete=models.CASCADE)
